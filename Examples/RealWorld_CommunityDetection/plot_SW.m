%% Experiments for the Southern Women Events Participation Dataset
% (c) Bloechl, Amjad, Geiger, 2017.
clear all; close all; clc;

load SW
color={'red','blue','teal','orange'};

%% Barber's 4x4 co-clustering
V_X_Barber=[1 1 1 1 1 1 2 3 2 2 4 4 4 4 4 3 3 3];
V_Y_Barber=[1 1 1 1 1 1 2 2 3 4 3 4 4 4];
V_X_true=zeros(18,4); V_Y_true=zeros(14,4);
for ind=1:18
    V_X_true(ind,V_X_Barber(ind))=1;
    colorX{ind}=color{V_X_Barber(ind)};
end
for ind=1:14
    V_Y_true(ind,V_Y_Barber(ind))=1;
    colorY{ind}=color{V_Y_Barber(ind)};
end

% Co-Clustering: AnnITCC with 50 restarts
% We have identical results for beta in [1,0.6-0.9,0.4-0.5,0-0.3]: Select
% 1, 0.7, 0.5 and 0 
[ V_X_collected,V_Y_collected, ~, beta_collected ] = Generalized_ITCC_annealing( SW/sum(sum(SW)), 4, 4,50 );

% For comparison: Co-Clustering with sGITCC; the results are slightly better
% for beta=0.9
% for ind=1:length(beta_collected)
%     [ V_X_collected(:,:,ind), V_Y_collected(:,:,ind), cost_min ] = Generalized_ITCC( SW/sum(sum(SW)), 4, 4,beta_collected(ind),50 );
% end

% Compute MAP values
for bind=1:length(beta_collected)
    MAP(1,bind)=micro_prec(V_X_collected(:,:,bind).'*V_X_true);
    MAP(2,bind)=micro_prec(V_Y_collected(:,:,bind).'*V_Y_true);
end
MAP

% Generate TikZ code for these settings
printTiKz(V_X_collected(:,:,1),V_Y_collected(:,:,1),colorX,colorY,'Barber4410.tex',11.5,0.5);
printTiKz(V_X_collected(:,:,4),V_Y_collected(:,:,4),colorX,colorY,'Barber4407.tex',11.5,0.5);
printTiKz(V_X_collected(:,:,6),V_Y_collected(:,:,6),colorX,colorY,'Barber4405.tex',11.5,0.5);
printTiKz(V_X_collected(:,:,11),V_Y_collected(:,:,11),colorX,colorY,'Barber4400.tex',11.5,0.5);

%% Ground Truth 2x3 co-clustering
V_X_GT=[1 1 1 1 1 1 1 1 1 2 2 2 2 2 2 2 2 2];
V_Y_GT=[1 1 1 1 1 2 2 2 2 3 3 3 3 3];
V_X_true=zeros(18,2); V_Y_true=zeros(14,3);
for ind=1:18
    V_X_true(ind,V_X_GT(ind))=1;
    colorX{ind}=color{V_X_GT(ind)};
end
for ind=1:14
    V_Y_true(ind,V_Y_GT(ind))=1;
    colorY{ind}=color{V_Y_GT(ind)};
end

% Co-Clustering: AnnITCC with 50 restarts
% We have identical results for all beta
[ V_X_collected,V_Y_collected, ~, beta_collected ] = Generalized_ITCC_annealing( SW/sum(sum(SW)), 2, 3, 50 );

% For comparison: Co-Clustering with sGITCC; the results are the same, but
% worse for beta<0.3
% for ind=1:length(beta_collected)
%     [ V_X_collected(:,:,ind), V_Y_collected(:,:,ind), cost_min ] = Generalized_ITCC( SW/sum(sum(SW)), 2, 3,beta_collected(ind),50 );
% end

% Compute MAP values
for bind=1:length(beta_collected)
    MAP(1,bind)=micro_prec(V_X_collected(:,:,bind).'*V_X_true);
    MAP(2,bind)=micro_prec(V_Y_collected(:,:,bind).'*V_Y_true);
end
MAP

% Generate TikZ code
printTiKz(V_X_collected(:,:,1),V_Y_collected(:,:,1),colorX,colorY,'GroundTruth10.tex',11.5,0.5);