%% Block structure off-diagonal
% Setting: 90*90, 3x3, k=3:3:20
% The same random initialization is chosen for sGITCC and AnnITCC
% Note that AnnITCC admits producing results for a series of values of \beta at once.
% Keeping all intermediate co-clustering solutions, one obtains co-clusterings for all 
% parameter values in the set {1, 1-\Delta,1-2\Delta,\dots,\beta}. The obtained 
% clusterings are the same as one would get from restarting AnnITCC for each value 
% in this set (with the same random initial partition).
% By default, \Delta=0.1, hence we can generate results for \beta running
% from 0 to 1 in steps of 0.1 in a single simulation run.

clear all; clc; close all;

%Settings
Naverage=500;
beta=1:-0.1:0;
k=3:3:20;

% Seed the random number generator to get reproducible results
rng('default');
rng(1);

%% Generate Model
N_x=90; N_y=90; C_x=3; C_y=3;
l=30;

V_X_true=[ones(l,1)  zeros(l,2); zeros(l,1) ones(l,1) zeros(l,1); zeros(l,2) ones(l,1)];
V_Y_true=[ones(l,1)  zeros(l,2); zeros(l,1) ones(l,1) zeros(l,1); zeros(l,2) ones(l,1)];

for m=1:length(k)
    %% Generate Model
    A=[];
    a = [ones(1,k(m)) zeros(1,l-k(m))];
    for i= 0:l-1
        A = [A;circshift(a,i)];
    end
    A = A(:,end:-1:1);
    T00 = [A zeros(l,l) zeros(l,l); zeros(l,l) A zeros(l,l);zeros(l,l) zeros(l,l) A];
    P00=T00/sum(sum(T00));
    
    figure
    imagesc(P00)
    colormap(hot)
    axis equal
    axis tight
    colorbar('FontSize', 16)
    print(sprintf('Heatmap-k%d.eps',k(m)),'-depsc')

%% Co-Clustering and Saving Results

    for run=1:Naverage       
        
        % Obtain random initialization   
        [V_X_init,V_Y_init]=randomClustering(N_x,N_y,C_x,C_y);

        [V_X_collected{run},V_Y_collected{run}, cost_collected{run}, beta_collected{run} ] = Generalized_ITCC_annealing( P00, C_x, C_y, V_X_init,V_Y_init);

        for bind=1:length(beta)
           MAPX_ann(run,bind)= micro_prec(V_X_collected{run}(:,:,bind).'*V_X_true);
           MAPY_ann(run,bind)= micro_prec(V_Y_collected{run}(:,:,bind).'*V_Y_true);
           
           [ V_X_final{run,bind}, V_Y_final{run,bind}, cost_min(run,bind) ] = Generalized_ITCC( P00, C_x, C_y, beta(bind), V_X_init,V_Y_init );
           MAPX_seq(run,bind)= micro_prec(V_X_final{run,bind}.'*V_X_true);  
           MAPY_seq(run,bind)= micro_prec(V_Y_final{run,bind}.'*V_Y_true);  
        end
    end
    
    save(sprintf('Block-%d.mat',k(m)));
end

%% Plotting
addpath(genpath('matlab2tikz'))
k_plotvec=[3,6,15];
colors={'-b','-r','-g','-m'};

hX=figure;
Xline_vec=[];
for m_ind=1:length(k_plotvec)
    noise=k_plotvec(m_ind);
    legend_string{m_ind}=sprintf('k=%d',noise);
    
    load(sprintf('Block-%d.mat',k_plotvec(m_ind)));
    
    figure(hX);
    aX=shadedErrorBar(beta,MAPX_ann,{@mean,@std},'lineprops',{colors{m_ind},'LineWidth',2});
    Xline_vec=[Xline_vec, aX.mainLine];
    hold on
    shadedErrorBar(beta,MAPX_seq,{@mean,@std},'lineprops',{strcat('-',colors{m_ind}),'LineWidth',2});
  
end
figure(hX);
axis([0 1 0.0 1])
xlabel('$\beta$','Interpreter','latex');
legend(Xline_vec,legend_string,'Location','southeast','Interpreter','latex');
% matlab2tikz('MAPX_block.tex')

