clear all;
close all;
clc;
% *************************************************************************
%                              EXPLANATION
% *************************************************************************

% Newsgroup 20 (NG 20) experiment for the subsets "Binary", "Multi5" and "Multi10". 
% The NG20 dataset is available at http://qwone.com/~jason/20Newsgroups/
% The relevant files for this experiment are in the
% 20news-bydate-matlab.tgz folder. Please download this folder and extract
% it into a subfolder of this matlab path called "NG20".
% According to the above website, these are the 20 Newsgroups sorted by date, where
% duplicates and newsgroup-identifying headers were removed, resulting in
% 18846 documents in total. The dataset is further split into a training and test
% set, which is not relevant in this context (as we deal with unsupervised
% clustering). 
% For train/test set, the document label is available in files
% train/test.label. The data (document-by-term-matrix) is avaiable in
% test.data and train.data, respectively.
% For train/test set, the mapping between document label and the exact
% terminology of the newsgroup is available in files train/test.map.
% 
% E.g. the binary dataset consisting of the newsgroups
% "talk.politics.mideast" and "talk.politics.misc" corresponds to the NG labels 18 and 19.

% Please select the NG20 subset you wish to simulate 
NG_subset = 'multi5'; % 'binary', 'multi5' or 'multi10' allowed

% *************************************************************************
%                               PREPROCESSING
% *************************************************************************

% load the data and corresponding labels, data(:,1) indicates the document
% index, data(:,2) indicates the word index and data(:,3) indicates the
% number of co-occurrencies.
disp('Load and read Data for NG20 Dataset!');
data = load('NG20/test.data');
label = load('NG20/test.label');
% assign number of documents and the labels corresponding to the NG20
% subsets
% BINARY: randomly pick 250 documents of label 18 and 19, respectively
% MULTI5: randomly pick 100 documents of labels 2, 9, 10, 15 and 18
% MULTI10: randomly pick 50 documents of labels 1, 5, 7, 8, 11, 12, 13, 14, 15, 17
switch NG_subset
    case 'binary'
        nDocPerGroup = 250;
        docLabel = [18 19];
        nClustDoc = 2;
    case 'multi5'
        nDocPerGroup = 100;
        docLabel = [1 9 10 15 18];
        nClustDoc = 5;
    case 'multi10'
        nDocPerGroup = 50;
        docLabel = [1 5 7 8 11 12 13 14 15 17];
        nClustDoc = 10;
    otherwise
        assert(false, 'Error: Invalid NG20 subset !');
end

% find the indices of the documents corresponding to the specific labels
ind = cell(nClustDoc,1);
for nLabel = 1:nClustDoc
    ind{nLabel} = find(label == docLabel(nLabel));
end

% perform random permutation to pick index list
randperm_label = cell(nClustDoc,1);
for nLabel = 1:nClustDoc
    randperm_label{nLabel} = randperm(length(ind{nLabel}));
end

% select the first nDoc elemtents of every label
nDocs = nDocPerGroup*nClustDoc; % total number of documents
ind_sel = zeros(nDocs,1);
for nLabel = 1:nClustDoc
   ind_sel((nLabel-1)*nDocPerGroup+1 : nLabel*nDocPerGroup) = ind{nLabel}(randperm_label{nLabel}(1:nDocPerGroup));
end

% create doc-by-term matrix
disp(['Create Document-By-Term Matrix for Dataset ', NG_subset, ' !']);
nWords = max(data(:,2)); % total number of words for the selected subsets
T = zeros(nDocs, nWords);
for i = 1:nDocs
    temp = find(ind_sel(i) == data(:,1));
    T(i, data(temp,2)) = data(temp,3);
end
% clear unused data from workspace
clear data;clear label;clear ind;clear ind_sel;clear temp;clear randperm_label;clear nLabel;

% normalize T to define joint probability distribution P_DW
disp(['Construct Probability Table for Dataset ', NG_subset, ' !']);
P_DW = T/sum(sum(T));
% return the indices of the 2000 words with highest contribution to mutual information I(D;W)
% (feature extraction, this is consistent with the preprocessing from the paper)
[index] = highest_contribution_mutual_information(P_DW, 2000);
T_reduced = T(:,index); % new doc-by-term matrix only consideres these 2000 words
% construct probability table for the Dataset
P_binary = T_reduced/sum(sum(T_reduced));

clear T; clear P_DW;clear index;clear nWords;clear T_reduced; clear nDocs

% *************************************************************************
%                       ACTUAL ANN_ITCC SIMULATION
% *************************************************************************

disp('Run ANN_GITCC with 25 sIB restarts for both document and word clusters!');
% initialize simulation parameters
for i = 1:7
   for j = 1:10
      result(i,j).numWordClusters = 2^i;
      result(i,j).run = j;
      result(i,j).nRestarts =  25;
      result(i,j).dataset = NG_subset;
   end
end
nSteps = 21; % please make sure this is consistent with the code in function Generalized_ITCC_annealing.m (Delta has to be set to 0.05).
% perform the simulation
for i = 1:7
    for run = 1:10
        % for every run, initialize for beta=1 using 25 sIB restarts for both X and Y
        % separately (this can be done by calling sGITCC with setting the number of clusters
        % for the other variable to 1; this yields the right clustering solution, while the cost function term
        % is plus a constant offset (I(X;Y)) which does not affect the clustering solution; the cost function is 
        % re-evaluated in the AnnITCC function to provide the correct cost as an output).
        
        % Note that this initalization procedure is only slightly different than running sGITCC with 25 restarts and beta=1 (where also both the clusterings for X and Y are 
        % optimized independent on each other). However, for a given random initialization, the co-clustering solution found by applying
        % sGITCC for beta=1 is almost (only the difference in cost functions after an iteration and thus the convergence is influenced, which may result in more/less iterations 
        % and thus in different clusterings; at fixed number of iterations, there are no differences at all) identical to the two clustering solutions found by applying sIB for 
        % both the word and document clusterings separately. 
        % The real difference is that in the case of sGITCC initialization, the cost functions of different restarts
        % consists of the sum of the cost function obtained by two independent clusterings, while for sIB restarts the clustering of each individual variable
        % determines the cost. This admits searching a larger solution
        % space than restarting sGITCC 25 times, consider e.g. the following example: 
        % sGITCC: restart1: clustX (cost 0.1), clustY (cost 1), total cost (1.1)
        %         restart2: clustX (cost 1.2), clustY(cost 0.1), total cost (1.2)
        % => total cost for restart 1 is better => good clustering solution
        %    for X, bad one for Y
        % sIB   : restart1: clustX (cost 0.1), clustY (cost 1)
        %         restart2: clustX (cost 1.2), clustY(cost 0.1)
        % => for clustX restart 1 is chosen, for clustY restart 2 is chosen
        % => good clustering solution for both variables, better
        %    initialization than with sGITCC
        [ V_X_init, ~, ~ ] = Generalized_ITCC( P_binary, nClustDoc, 1, 1, result(i,run).nRestarts );
        [ ~, V_Y_init, ~ ] = Generalized_ITCC( P_binary, 1, result(i,run).numWordClusters, 1, result(i,run).nRestarts );
        % Run AnnITCC witht the above initializations. 
        % The exact parameters are Delta = 0.05, numITER = 20 and tol = 0.001 (please make sure they are 
        % correctly set in the functions Generalized_ITCC_annealing.m and sequential_main_loop.m). 
        [ V_X, ~, result(i,run).cost_collected, result(i,run).beta_collected] = Generalized_ITCC_annealing( P_binary, nClustDoc, result(i,run).numWordClusters, V_X_init, V_Y_init );
        % construct the crossover table for each step of beta (nSteps = 21 due to Delta = 0.05)
        result(i,run).cross_table = zeros(nClustDoc,nClustDoc,nSteps);
        for n = 1:nSteps
            for dimx = 1:nClustDoc
                for dimy = 1:nClustDoc
                    result(i,run).cross_table(dimx, dimy, n) = sum(V_X((dimx-1)*nDocPerGroup+1:dimx*nDocPerGroup,dimy,n));
                end
            end
            % calculate MAPR for each beta according to the above crossover table
            result(i,run).micro_precision(n) = micro_prec(result(i,run).cross_table(:,:,n));
        end
    end
    disp(['Created Results for ', num2str(result(i,1).numWordClusters), ' Word Clusters and Dataset "Binary"!']);
end
save('result.mat','result');

% *************************************************************************
%               POST PROCESSING AND PLOTTING THE RESULTS
% *************************************************************************

% compute average over the 10 runs of MAPR
mapr_avr = zeros(21,7);
for j = 1:7
    for i = 1:10
       mapr_avr(:,j) =  mapr_avr(:,j) + 1/10*result(j,i).micro_precision';
    end
end

% plot the results
figure;
for i = 1:6
  plot(result(j,1).beta_collected, mapr_avr(:,i), '-*');
  hold on
end
plot(result(j,1).beta_collected, mapr_avr(:,7), '-*');
ax = gca;
ax.XLim = [0 1];
legend('2 Word Cluster', '4 Word Cluster', '8 Word Cluster', '16 Word Cluster', ...
    '32 Word Cluster', '64 Word Cluster', '128 Word Cluster');
title(['MAPR for Dataset', NG_subset, ' !']);

% end of simulation
disp(['NG 20 Simulation for Dataset ', NG_subset, ' completed !']);