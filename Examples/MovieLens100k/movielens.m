%MovieLens100k experiment. 
%The MovieLens100k dataset is available at https://grouplens.org/datasets/movielens/100k/
%The relevant files for this experiment are u.data containing the user
%ratings and u.item containing the genre information for the movies. The
%details about the structure of the files can be found in MovieLens100k
%README file. Add these files to the same folder as this .m file before
%running the code. 
%The experiments are peformed for 10 user clusters and 19 movie clusters.
%We do 25 random restarts for beta=1 and choose the best w.r.t cost to
%initialize AnnITCC. 
clear;
clc;

%import the user-movie-rating data. rating_data(:,1) are the user ids,
%rating_data(:,2) are the movie ids and  rating_data(:,3) are the ratings.
rating_data = importdata('u.data');
%Constructing user-movie rating matrix R
R = zeros(max(rating_data(:,1)),max(rating_data(:,2)));
for i=1:length(rating_data)
    R(rating_data(i,1),rating_data(i,2)) = rating_data(i,3);
end
P = R/sum(sum(R)); %normalizing R to define joint probability distribution
%number of user clusters and number of movie clusters
num_userclusters =10;
num_movieclusters =19;
%Co-clustering using AnnITCC with 25 restarts at beta=1 for initialization
[user_clusters,movie_clusters, cost_collected, beta_collected ] = Generalized_ITCC_annealing(P, num_userclusters, num_movieclusters, 25);
%saving the clustering results. 
save('clusters.mat','user_clusters','movie_clusters','beta_collected','cost_collected');


%importing movie genre ground truth
genre_data = importdata('u.item','|'); 
MAP_genre = zeros(1,length(beta_collected));
%computing MAP' value for movie_clusters when compared to ground truth
%extracted from u.item
for beta_index=1:length(beta_collected)  
    temp_summations=0;        %counting the max number of movies in each cluster belonging to the same genre summed up over all movie clusters.
    for i=1:size(movie_clusters,2)  %iterating over all 19 movie clusters
        temp_movie_cluster =movie_clusters(:,i,beta_index);
        temp_max_index =1; % to keep the index of the genre which matches the current movie_cluster best
        temp_max_value =0; % to keep the number of movies that match the best matched genre
        for j=1:size(genre_data.data,2)  %iterating over all the genres 
            if sum(temp_movie_cluster.*genre_data.data(:,j)) > temp_max_value  % sum(movie_cluster.*data.data(:,j)) represents the number of movies in the current cluster that match the jth genre
                temp_max_index =j;
                temp_max_value = sum(temp_movie_cluster.*genre_data.data(:,j));
            end
        end
        temp_summations = temp_summations + temp_max_value;
    end
    MAP_genre(beta_index) = temp_summations./length(genre_data.data); % normalizing total number of movies that match a certain genre summed up over all clusters by the total number of movies=1682
end

plot(beta_collected,MAP_genre);

%Computing the MAP' value for randomly generated movie clustering. 
movie_cluster_random = zeros(size(movie_clusters(:,:,1)));
for i=1:length(genre_data.data)
    movie_cluster_random(i,randi(19)) = 1;
end

temp_summations=0;  %counting the max number of movies in each cluster belonging to the same genre summed up over all movie clusters.
for i=1:size(movie_cluster_random,2) %iterating over all 19 clusters
    temp_movie_cluster =movie_cluster_random(:,i);
    temp_max_index =1; % to keep the index of the genre which matches the current random cluster best
    temp_max_value =0; % to keep the number of movies that match the best matched genre
    for j=1:size(genre_data.data,2) %iterating over all the genres defined in u.item
        if sum(temp_movie_cluster.*genre_data.data(:,j)) > temp_max_value  % sum(movie_cluster.*data.data(:,j)) represents the number of movies in the current cluster that match the jth genre
            temp_max_index =j;
            temp_max_value = sum(temp_movie_cluster.*genre_data.data(:,j));
        end
    end
    temp_summations = temp_summations + temp_max_value;
end
MAP_random = temp_summations./length(genre_data.data); % normalizing total number of movies that match a certain genre summed up over all clusters by the total number of movies=1682

