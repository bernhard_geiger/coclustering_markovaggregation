%% Block structure with noise
% Setting: 80*50, 5x3, eps=0:0.01:1
% The same random initialization is chosen for sGITCC and AnnITCC
% Note that AnnITCC admits producing results for a series of values of \beta at once.
% Keeping all intermediate co-clustering solutions, one obtains co-clusterings for all 
% parameter values in the set {1, 1-\Delta,1-2\Delta,\dots,\beta}. The obtained 
% clusterings are the same as one would get from restarting AnnITCC for each value 
% in this set (with the same random initial partition).
% By default, \Delta=0.1, hence we can generate results for \beta running
% from 0 to 1 in steps of 0.1 in a single simulation run.

clear all; clc; close all;

%Settings
Naverage=500;
beta=1:-0.1:0;
noise_vec=0:0.1:1;

% Seed the random number generator to get reproducible results
rng('default');
rng(1);

%% Generate Model
% Rectangular model
N_x=80; N_y=50; C_x=5; C_y=3;

T00=[2*ones(15,15) ones(15,15) zeros(15,20);
    zeros(10,15)  2*ones(10,15) ones(10,20);
    ones(20,15) zeros(20,15) 2*ones(20,20);
    1.5*ones(5,15) 0.5*ones(5,15) ones(5,20);
    0.5*ones(30,15) ones(30,15) 1.5*ones(30,20)];
T00=T00/sum(sum(T00));

N=rand(N_x,N_y,Naverage);
for ind=1:Naverage
    N(:,:,ind)=N(:,:,ind)/sum(sum(N(:,:,ind)));
end

V_X_true=[ones(15,1) zeros(15,4); zeros(10,1) ones(10,1) zeros(10,3); zeros(20,2) ones(20,1) zeros(20,2); zeros(5,3) ones(5,1) zeros(5,1); zeros(30,4) ones(30,1)];
V_Y_true=[ones(15,1) zeros(15,2); zeros(15,1) ones(15,1) zeros(15,1); zeros(20,2) ones(20,1)];

%% Co-Clustering and Saving Results
for noise=noise_vec
    for run=1:Naverage
        P00=(1-noise)*T00+noise*N(:,:,run);
        if run==1
            figure
            imagesc(P00)
            colormap(hot)
            axis equal
            axis tight
            colorbar
            print(sprintf('Heatmap-n%1.1f.eps',noise),'-depsc')
        end
        
        % Obtain random initialization   
        [V_X_init,V_Y_init]=randomClustering(N_x,N_y,C_x,C_y);

        [V_X_collected{run},V_Y_collected{run}, cost_collected{run}, beta_collected{run} ] = Generalized_ITCC_annealing( P00, C_x, C_y, V_X_init,V_Y_init);

        for bind=1:length(beta)
           MAPX_ann(run,bind)= micro_prec(V_X_collected{run}(:,:,bind).'*V_X_true);
           MAPY_ann(run,bind)= micro_prec(V_Y_collected{run}(:,:,bind).'*V_Y_true);
           
           [ V_X_final{run,bind}, V_Y_final{run,bind}, cost_min(run,bind) ] = Generalized_ITCC( P00, C_x, C_y, beta(bind), V_X_init,V_Y_init );
           MAPX_seq(run,bind)= micro_prec(V_X_final{run,bind}.'*V_X_true);  
           MAPY_seq(run,bind)= micro_prec(V_Y_final{run,bind}.'*V_Y_true);  
        end
    end
    
    save(sprintf('Noisy-%1.1f.mat',noise));
end

%% Plotting
addpath(genpath('matlab2tikz'))
noise_plotvec=[0, 0.5, 0.7, 0.8];
colors={'-b','-r','-g','-m'};

hX=figure;
hY=figure;
Xline_vec=[];
Yline_vec=[];
for noise_ind=1:length(noise_plotvec)
    noise=noise_plotvec(noise_ind);
    legend_string{noise_ind}=sprintf('$\\epsilon=%0.1f$',noise);
    
    load(sprintf('Noisy-%1.1f.mat',noise));
    
    figure(hX);
    aX=shadedErrorBar(beta,MAPX_ann,{@mean,@std},'lineprops',{colors{noise_ind},'LineWidth',2});
    Xline_vec=[Xline_vec, aX.mainLine];
    hold on
    
    figure(hY);
    aY=shadedErrorBar(beta,MAPY_ann,{@mean,@std},'lineprops',{colors{noise_ind},'LineWidth',2});
    Yline_vec=[Yline_vec, aY.mainLine];
    hold on
end
figure(hX);
axis([0 1 0.4 1])
xlabel('$\beta$','Interpreter','latex');
legend(Xline_vec,legend_string,'Location','southeast','Interpreter','latex');
% matlab2tikz('MAPX.tex')

figure(hY);
axis([0 1 0.4 1])
xlabel('$\beta$','Interpreter','latex');
legend(Yline_vec,legend_string,'Location','southeast','Interpreter','latex');
% matlab2tikz('MAPY.tex')

h0=figure;
load('Noisy-0.0.mat')
aX0ann=shadedErrorBar(beta,MAPX_ann,{@mean,@std},'lineprops',{'b-','LineWidth',2});
hold on
aX0seq=shadedErrorBar(beta,MAPX_seq,{@mean,@std},'lineprops',{'b--','LineWidth',2});
load('Noisy-0.7.mat')
aX7ann=shadedErrorBar(beta,MAPX_ann,{@mean,@std},'lineprops',{'r-','LineWidth',2});
aX7seq=shadedErrorBar(beta,MAPX_seq,{@mean,@std},'lineprops',{'r--','LineWidth',2});
axis([0 1 0 1])
xlabel('\beta','Interpreter','latex');
% matlab2tikz('MAPX_seq.tex')

% Figure not shown in the paper. A comparison of costs for solutions found
% by our algorithms and the costs of the ground truth. Notable values for
% noise: 0.5, 0.7, 0.8. These results are evidence for some statements that
% we make in the paper.
load('Noisy-0.1.mat')
for run=1:Naverage
    P00=(1-noise)*T00+noise*N(:,:,run);

    P_XclustY = P00*V_Y_true; % joint distribution between X and the clustered Y
    P_clustXY = V_X_true'*P00; % joint distribution between the clustered X and Y
    P_clustXclustY = V_X_true'*P_XclustY;
    I_clustXclustY = mutual_information(P_clustXclustY);
    I_XclustY = mutual_information(P_XclustY);
    I_clustXY = mutual_information(P_clustXY);
    I_XY = mutual_information(P00);
    for bind=1:length(beta)
        beta_value=beta(bind);
        factor_1 = 1-2*beta_value;
        factor_2 = 2*(1-beta_value);
        cost_true(run,bind) = - (-(factor_1*(I_XclustY + I_clustXY) - factor_2*I_clustXclustY )) + 2*beta_value*I_XY;
        cost_result(run,bind)=cost_collected{run}(bind);
    end
end

figure
subplot(2,1,1)
aX1=shadedErrorBar(beta,cost_result,{@mean,@std},'lineprops',{'b-','LineWidth',2});
hold on
aX3=shadedErrorBar(beta,cost_min,{@mean,@std},'lineprops',{'y-','LineWidth',2});
aX2=shadedErrorBar(beta,cost_true,{@mean,@std},'lineprops',{'r-','LineWidth',2});
xlabel('$\beta$','Interpreter','latex');
ylabel('$L_\beta$','Interpreter','latex');
legend([aX1.mainLine,aX3.mainLine,aX2.mainLine],{'Solution found by AnnITCC','Solution found by sGITCC','Ground Truth'})
subplot(2,1,2)
aX1=shadedErrorBar(beta,cost_result-cost_true,{@mean,@std},'lineprops',{'b-','LineWidth',2});
hold on
aX3=shadedErrorBar(beta,cost_min-cost_true,{@mean,@std},'lineprops',{'y-','LineWidth',2});
aX2=shadedErrorBar(beta,cost_true-cost_true,{@mean,@std},'lineprops',{'r-','LineWidth',2});
xlabel('$\beta$','Interpreter','latex');
ylabel('$\Delta L_\beta$','Interpreter','latex');
legend([aX1.mainLine,aX3.mainLine,aX2.mainLine],{'Solution found by AnnITCC','Solution found by sGITCC','Ground Truth'})
