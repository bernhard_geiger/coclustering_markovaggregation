%% Pathological examples 
% These examples illustrate that even for a clear co-clustering structure
% the ground truth need not coincide with the global optimum of the cost
% function, depending on the range of beta.
clc;
clear all;

% correct clustering is global optimum for beta = 0.5
P_test = [0.12 0    0
          0    0.39 0.05
          0    0.05 0.39];
suffix='1';

% incorrect clustering is global optimum for beta = 0.5
P_test = [0.12 0    0
          0    0.4 0.04
          0    0.04 0.4];
suffix='2';

%ground truth clustering
V_row_correct = [1 0
                 0 1
                 0 1];
             
V_col_correct = [1 0
                 0 1
                 0 1];

%incorrect clustering
V_row_incorrect = [1 0
                   1 0
                   0 1];
V_col_incorrect = [1 0
                   1 0
                   0 1];
beta=0:0.1:1;
cost_correct =zeros(1,length(beta));
cost_incorrect =zeros(1,length(beta));

% I(X;Y)
I_P = mutual_information(P_test);

% compute quantities for correct clustering
P_col_correct = P_test*V_col_correct;
P_row_correct = V_row_correct'*P_test;
P_reduced_correct = V_row_correct'*P_col_correct;
I_reduced_correct = mutual_information(P_reduced_correct); %I(Xclust;Yclust)
I_col_correct = mutual_information(P_col_correct); %I(X;Yclust)
I_row_correct = mutual_information(P_row_correct); %I(Xclust;Y)

% compute quantities for incorrect clustering
P_col_incorrect = P_test*V_col_incorrect;
P_row_incorrect = V_row_incorrect'*P_test;
P_reduced_incorrect = V_row_incorrect'*P_col_incorrect;
I_reduced_incorrect = mutual_information(P_reduced_incorrect); %I(Xclust;Yclust)
I_col_incorrect = mutual_information(P_col_incorrect); %I(X;Yclust)
I_row_incorrect = mutual_information(P_row_incorrect); %I(Xclust;Y)

for index=1:length(beta)
    factor_1 = 1-2*beta(index);
    factor_2 = 2*(1-beta(index));
  
    cost_correct(index) = -(-(factor_1*(I_col_correct + I_row_correct) - factor_2*I_reduced_correct)) + 2*beta(index)*I_P ;
    cost_incorrect(index) = -(-(factor_1*(I_col_incorrect + I_row_incorrect) - factor_2*I_reduced_incorrect)) + 2*beta(index)*I_P;
end


plot(beta, cost_correct,'k'); 
hold on;
plot(beta, cost_incorrect,'--r');
xlabel('$\beta$','Interpreter','latex');
ylabel('$L_\beta$','Interpreter','latex');
% matlab2tikz(sprintf('Cost_%s.tex',suffix))
