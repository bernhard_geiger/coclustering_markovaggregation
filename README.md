# README #

### What is this repository for? ###

* This repository contains the code necessary to run examples shown in the paper ["Co-Clustering via Information-Theoretic Markov Aggregation"](https://arxiv.org/abs/1801.00584) by Bloechl, Amjad, and Geiger.
* For visualization of the results, you need to include [Rob Campbell's shadedErrorBar](https://github.com/raacampbell/shadedErrorBar)
* For converting figures to tex-files (as we did for the paper), you need to include [matlab2tikz](https://github.com/matlab2tikz/matlab2tikz) 
* For the MovieLens100k experiments, please download the dataset from [grouplens.org/](https://grouplens.org/datasets/movielens/100k/). The relevant files are "u.data" and "u.item".
* The datasets for Newsgroup20 experiments are available at [qwone.com/](http://qwone.com/~jason/20Newsgroups/), where the relevant files are in the 20news-bydate-matlab.tgz folder (further instructions are given in our MATLAB code).

### Contribution guidelines ###

You are welcome to play around with the code and improve it! If you want to actively contribute, just give us a ping.

### Who do I talk to? ###

We are [Clemens Bloechl](mailto:clemens.bloechl@web.de), Rana Ali Amjad, and [Bernhard C. Geiger](http://entropictalks.blogspot.com/).