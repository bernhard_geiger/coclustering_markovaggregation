function [ micro_precision ] = micro_prec( C )
% Compute the micro-averaged precision from the confusion matrix C

nC = size(C,2);
v = 1:nC;
P = perms(v);
nP = size(P,1);
correct = zeros(1,nP);
for i = 1:nP
    correct(i) = trace(C(:, P(i,:)));
end
micro_precision = max(correct)/sum(sum(C));
end

