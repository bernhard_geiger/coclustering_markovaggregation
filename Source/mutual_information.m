function [ I_XY ] = mutual_information( P_XY )
% [ I_XY ] = mutual_information( P_XY )
% This function calculates the mutual information between two discrete RVs
% X any Y given their joint distribution P_XY.
%
% Input: P_XY
% 
% Output: I_XY (mutual information between X and Y)

%#codegen
coder.inline('never')

% marginal distribution of X
P_X = sum(P_XY,2);
% marginal distribution of X
P_Y = sum(P_XY);
% calculate mutual information 
I_XY = 0;
for i = 1:length(P_X)
    for j = 1:length(P_Y)
        if P_XY(i,j) ~= 0
           I_XY = I_XY + P_XY(i,j)*log(P_XY(i,j)/(P_Y(j)*P_X(i)));
        end
    end
end
I_XY = I_XY/log(2);
end

