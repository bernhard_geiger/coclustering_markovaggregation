function [ V_X_final, V_Y_final, cost_min ] = Generalized_ITCC( P_XY, numClustX, numClustY, beta, varargin )
% [ V_X_final, V_Y_final, cost_min ] = Generalized_ITCC( P_XY, numClustX, numClustY, beta, varargin )
% Main Function of the Generalized Information-Theoretic Co-Clustering Algorithm.
% The function either takes nStarts random initalizations of the clusters of X and Y (clust X and clustY)
% or it takes given clustering matrix initializations depending on the
% number of input arguments. The following inputs have to be set: 
% 
% * P_XY: joint distribution between X and Y
% * numClustX: number of clusters for variable X 
% * numClustY: number of clusters for variable Y
% * beta: parameter for balancing the different terms of the cost function
% * in case of one further input argument: varargin{1} is the number
%   nStarts of restarts for random initializations of the clusters of X and Y
% * in case of two further input arguments: varargin{1} is the initial clustering
%   matrix for X, varargin{2} the initial clustering matrix for Y (for the format, see
%   the explanations for the function outputs)
%
% The following outputs are generated.
% * V_X_final: final clustering matrix for X. V_X(i,j) assigns that i in X is assigned
%   to cluster j in clustX
% * V_Y_final: final clustering matrix for Y. V_Y(i,j) assigns that i in Y is assigned
%   to cluster j in clustY
% * cost_min: final value of the cost function at the above
%   clustering solution.


if nargin==5
    nStarts=varargin{1};
else
    nStarts = 1;
end
% number of elements from X and Y, respectively
numX = size(P_XY,1);
numY = size(P_XY,2);
% collect all clustering matrices obtained after optimizing the random
% initializations (meaning of the clustering matrices is explained
% in the sequential_opt_clustX and sequential_opt_clustY functions...)
V_X_collected = zeros(numX, numClustX,nStarts);
V_Y_collected = zeros(numY, numClustY,nStarts);
cost_collected = zeros(nStarts, 1);

for i = 1:nStarts
    if nargin==6 % interpret the input arguments as clustering matrices
        V_X_init=varargin{1};
        V_Y_init=varargin{2};
    else % random initialization 
        ds = randi([1 numClustX], numX,1);
        dw = randi([1 numClustY], numY, 1);
        V_X_init = zeros(numX, numClustX);
        V_Y_init = zeros(numY, numClustY);
        for q = 1:numX
            V_X_init(q, ds(q)) = 1;
        end
        for q = 1:numY
            V_Y_init(q, dw(q)) = 1;
        end
    end
    [V_X, V_Y, cost] = sequential_main_loop( V_X_init, V_Y_init, P_XY, beta);
    V_X_collected(:,:,i) = V_X;
    V_Y_collected(:,:,i) = V_Y;
    cost_collected(i) = max(cost);
end
[~, index] = max(cost_collected);
% assign final clustering solutions s.t. they are optimal w.r.t. the cost...
V_X_final = V_X_collected(:,:,index);
V_Y_final = V_Y_collected(:,:,index);

% calculations for finally assigning the cost (using the actual cost function dependent on beta)
P_XclustY = P_XY*V_Y_final; % joint distribution between X and the clustered Y
P_clustXY = V_X_final'*P_XY; % joint distribution between the clustered X and Y
P_clustXclustY = V_X_final'*P_XclustY;
I_clustXclustY = mutual_information(P_clustXclustY);
I_XclustY = mutual_information(P_XclustY);
I_clustXY = mutual_information(P_clustXY);
I_XY = mutual_information(P_XY); % I_XY does not change during clustering

factor_1minus2beta = 1-2*beta;
factor_2minus2beta = 2*(1-beta);
cost_min = - (-(factor_1minus2beta*(I_XclustY + I_clustXY) - factor_2minus2beta*I_clustXclustY )) + 2*beta*I_XY;

end