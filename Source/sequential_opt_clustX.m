function [ V_X, I_clustXY, P_clustXY] = sequential_opt_clustX( factor_1minus2beta, factor_2minus2beta, V_Y, V_X, P_XY)
% [ V_X, I_clustXY, P_clustXY] = sequential_opt_clustX( factor_1minus2beta, factor_2minus2beta, V_Y, V_X, P_XY)
% This function does one iteration in one-sided optimization of the
% clustering of variable X. The following inputs have to be set:
%
% * factor_1minus2beta: 1-2*beta is the first scaling factor for the cost
%   function
% * factor_2minus2beta: 2-2*beta is the second scaling factor for the cost
%   function
% * V_Y: clustering matrix of Y. V_Y(i,j) assigns that i in Y is assigned
%   to cluster j in clustY
% * V_X: clustering matrix of Y. V_X(i,j) assigns that i in X is assigned
%   to cluster j in clustX
% * P_XY: joint distribution between X and Y.
%
% The function creates the following outputs: 
% 
% * V_X: updated clustering matrix of X
% * I_clustXY: mutual information between the clustered X and Y
% * P_clustXY: joint distribution between the clustered X and Y

card_X = size(P_XY,1); % cardinality of X
card_clustX = size(V_X,2); % cardinality of the clustered X (:= number of clusters for X)
P_XclustY = P_XY*V_Y; % distribution between X and the clustered Y
P_Y = sum(P_XY); % marginal distribution of Y

cost_possible_clustX = zeros(card_clustX,1); % cost for each possible cluster of X

% for every element i in X do the following:
for i = 1:card_X
    % remove element i from its current cluster (eliminating a row in V_X means
    % assigning it to no cluster at all)
    V_X(i,:) = zeros(1,card_clustX);
    % compute P_clustXY_without_i which is the equal to the difference between the 
    % distribution of the clustered X and Y and the contribution of element
    % i to this distribution; analog, compute P_clustXclustY_without_i
    V_X_sparse = sparse(V_X');
    P_clustXY_without_i = V_X_sparse*P_XY;
    P_clustXclustY_without_i = V_X_sparse*P_XclustY;
    % calculate the contribution of each cluster j to I(clustX;Y) and
    % I(clustX;Y) (all terms minus the contribution of element i)
    I_clustXY_without_i_clustj = zeros(1,card_clustX);
    for j = 1:card_clustX
        I_clustXY_without_i_clustj(j) = delta_mutual_information(P_clustXY_without_i,j, P_Y);
    end
    I_clustXY_without_i = sum(I_clustXY_without_i_clustj);
    % for every possible cluster j in clustX
    for j = 1:card_clustX
        P_clustXY = P_clustXY_without_i;
        P_clustXclustY = P_clustXclustY_without_i;
        % preliminarily putting element i in cluster j is equal to adding
        % the i'th row of P_XY to the j'th row of P_clustXY. Note that now
        % P_clustXY is a probability distribution
        P_clustXY(j,:) = P_clustXY(j,:) + P_XY(i,:);
        P_clustXclustY(j,:) = P_clustXclustY(j,:) + P_XclustY(i,:);
        I_clustXclustY = mutual_information(P_clustXclustY);
        % calculate the correct contribution of cluster j including element i to I(clustX;Y)
        delta_I_clustXY = delta_mutual_information(P_clustXY,j, P_Y);
        % calculate I(clustX;Y) as the precalculated I(clustX;Y) minus the
        % contribution of element i minus the contribution of cluster j
        % without element i plus the contribution of cluster j including element i
        I_clustXY = delta_I_clustXY + I_clustXY_without_i - I_clustXY_without_i_clustj(j);
        % calculate the cost of putting element i in cluster j (independent terms neglected)
        cost_possible_clustX(j) = -(factor_1minus2beta*I_clustXY - factor_2minus2beta*I_clustXclustY);
    end
    % find cluster index that maximizes the cost and assign the element i
    % to index
    [~, index] = max(cost_possible_clustX);
    V_X(i,index) = 1;
end
P_clustXY = V_X'*P_XY;
I_clustXY = mutual_information(P_clustXY);
end