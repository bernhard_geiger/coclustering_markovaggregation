function [ V_X_collected,V_Y_collected, cost_collected, beta_collected] = Generalized_ITCC_annealing( P_XY, numClustX, numClustY, varargin )
% [ V_X_collected,V_Y_collected, cost_collected, beta_collected] = Generalized_ITCC_annealing( P_XY, numClustX, numClustY, varargin )
% Beta - Annealing: Starts with beta = 1 and subsequently reduces beta,
% where the clustering results of the previous step serve as an input to
% the subsequent clusterings. The following inputs have to be set: 
%
% * P_XY: joint distribution between X and Y
% * numClustX: number of clusters for variable X 
% * numClustY: number of clusters for variable Y
% * case of no further input argument: sGITCC with 1 restart is performed
%   as initialization
% * in case of one further input argument: varargin{1} is the number
%   nStarts of restarts for random initializations of the clusters of X and Y
% * in case of two further input arguments: varargin{1} is the initial clustering
%   matrix for X, varargin{2} the initial clustering matrix for Y (for the format, see
%   the explanations for the function outputs).
% 
% The function creates the following outputs: 
% 
% * V_X_collected: the clustering matrices for X and all steps of beta. V_X(i,j) assigns that 
%   i in X is assigned to cluster j in clustX
% * V_Y_collected: the clustering matrices for Y and all steps of beta. V_Y(i,j) assigns that 
%   i in Y is assigned to cluster j in clustY
% * cost_collected: the value of the cost function with above clustering
%   solutions at each value of beta
% * beta_collected: the value of beta at each step.
% 
% The following parameter Delta assigns the distance between two subsequent
% samples of beta and may be modified.
Delta = 0.1; % distance between two subsequent samples of beta

numX = size(P_XY,1);
numY = size(P_XY,2);
beta = 1;
% collect all clustering matrices obtained after optimizing with a decreased beta
% (meaning of the clustering matrices is explained
% in the sequential_opt_clustX and sequential_opt_clustY functions...)
% number of steps in which beta is decreased...
nSteps = 1/Delta+1;
V_X_collected = zeros(numX, numClustX,nSteps); % clustering matrix for X and each step
V_Y_collected = zeros(numY, numClustY,nSteps); % clustering matrix for Y and each step
cost_collected = zeros(nSteps, 1); % value of cost function at each step
if nargin==3    % standard
    [ V_X_init, V_Y_init, cost_init ] = Generalized_ITCC( P_XY, numClustX, numClustY, beta );
elseif nargin==4    % restart varargin{1} times for beta=1, choose the best
    [ V_X_init, V_Y_init, cost_init ] = Generalized_ITCC( P_XY, numClustX, numClustY, beta, varargin{1} );
else % initialize with clusterings varargin{1,2}
    [ V_X_init, V_Y_init, cost_init ] = Generalized_ITCC( P_XY, numClustX, numClustY, beta, varargin{1},varargin{2} );
end
V_X_collected(:,:,1) = V_X_init; 
V_Y_collected(:,:,1) = V_Y_init;
cost_collected(1) = cost_init;
beta_collected = zeros(nSteps, 1); % value of beta at each step
beta_collected(1) = beta;
% now at each step, redulce beta by Delta (until beta=0 is reached) and perform Generalized ITCC with
% the previous clustering solution as initialization. 
for i = 2:nSteps
    beta = beta - 1/(nSteps-1);
    beta_collected(i) = beta;
    [V_X, V_Y, cost] = Generalized_ITCC( P_XY, numClustX, numClustY, beta, V_X_init,V_Y_init );
    V_X_collected(:,:,i) = V_X;
    V_Y_collected(:,:,i) = V_Y;
    V_X_init = V_X;
    V_Y_init = V_Y;
    cost_collected(i) = cost;
end

end

