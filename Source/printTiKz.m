function printTiKz(V_X,V_Y,colorX,colorY,filename,startheight,spacing)
% This function generates TikZ code for the bipartite graph corresponding
% to the Southern Women Event Participation Dataset. It separates nodes
% based on the event cluster and community structure, and colors them
% depending on the color vectors passed as arguments.
% (c) Bernhard Geiger, 2017
fid=fopen(filename,'w');

height=startheight;
for indc=1:size(V_X,2)
    rowind=find(V_X(:,indc)==1);
    for indr=1:length(rowind)
        fprintf(fid,'\\vertex (W%d) at (0,%0.1f) [%s]{%02d};\n',rowind(indr),height,colorX{rowind(indr)},rowind(indr));
        height=height-spacing;
    end
    height=height-((startheight-spacing*(size(V_X,1)-1))/(size(V_X,2)-1));
end

height=startheight;
for indc=1:size(V_Y,2)
    rowind=find(V_Y(:,indc)==1);
    for indr=1:length(rowind)
        fprintf(fid,'\\vertex (E%d) at (8,%0.2f) [%s]{%02d};\n',rowind(indr),height,colorY{rowind(indr)},rowind(indr));
        height=height-spacing;
    end
    height=height-((startheight-spacing*(size(V_Y,1)-1))/(size(V_Y,2)-1));
end

fclose(fid);