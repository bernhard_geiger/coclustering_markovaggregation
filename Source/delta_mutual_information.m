function [ I ] = delta_mutual_information( P_XY, i , P_Y)
% calculates the contribution to mutual information I(X;Y) of element X_i

%#codegen
coder.inline('never')

P_X_i = sum(P_XY(i,:));
dI = 0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%% calculate dI1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for j = 1:length(P_Y)
    if P_XY(i,j) ~= 0
        dI = dI + P_XY(i,j)*log(P_XY(i,j)/(P_Y(j)*P_X_i));
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% whole factor that changes for each row
I = dI /log(2);

end
