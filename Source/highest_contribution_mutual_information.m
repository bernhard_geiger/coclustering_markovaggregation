function [ index_num ] = highest_contribution_mutual_information( P_XY, num )
% [ index_num ] = highest_contribution_mutual_information( P_XY, num )
% this function returns the indices of the "num" indices of Y with hightest
% contribution to the overall mutual information I(X;Y). The following
% inputs have to be set: 
% 
% * P_XY : joint distribution between two discrete RVs X and Y
% * num  : the "num" indices with highest contribution to mutual information
%          shall be returned.
% 
% The function generates the following output:
% * index_num: the first "num" indices of Y with highest contribution to
%   I(X;Y).

nY = size(P_XY,2); % number of elements in Y
nX = size(P_XY,1); % number of elements in X

P_X = sum(P_XY,2); % marginal distribution of X
P_Y = sum(P_XY,1); % marginal distribution of Y

contr = zeros(nY, 1); % initialize contribution to mutual information

% evaluate the contribution to overall mutual information for each element
% in Y
for i = 1:nY
    for j = 1:nX
        if P_XY(j,i) ~= 0
            contr(i) = contr(i) +  P_XY(j,i)*log2(P_XY(j,i)/P_X(j)/P_Y(i)); 
        end
    end
end
% sort the results in descending order and return the first "num" indices
[~, index] = sort(contr, 'descend');
index_num = index(1:num);

end
