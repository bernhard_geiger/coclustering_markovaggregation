function [V_X, V_Y, Cost_k] = sequential_main_loop( V_X, V_Y, P_XY, beta  )
% [V_X, V_Y, Cost_k] = sequential_main_loop( V_X, V_Y, P_XY, beta  )
% This function performs all iterations of sequentially clustering X and Y
% with a predefined beta and initial clusterings. The following inputs have
% to be provided: 
% 
% * V_Y: clustering matrix of Y. V_Y(i,j) assigns that i in Y is assigned
%   to cluster j in clustY
% * V_X: clustering matrix of Y. V_X(i,j) assigns that i in X is assigned
%   to cluster j in clustX
% * P_XY: joint distribution between X and Y.
% * beta: parameter for balancing the cost function
% 
% The function creates the following outputs: 
% 
% * V_X: updated clustering matrix of X
% * V_Y: updated clustering matrix of Y
% * Cost_k: factors assigning the cost at each iteration (not the actual cost function)
% 
% The parameters numITER (:= number of iterations) and tol (threshold at which by definition
% convergence is reached) are defined below and may be modified. 
numITER = 20;
tol = 0.001;

% the factors dependent on beta for evaluating the cost function at each iteration
factor_1minus2beta = 1-2*beta;
factor_2minus2beta = 2*(1-beta);
% joint distribution between X and the clustered Y
P_XclustY = P_XY*V_Y;
% joint distribution between the clustered X and Y
P_clustXY = V_X'*P_XY;
% joint distribution between the clustered X and the clustered Y
P_clustXclustY = V_X'*P_XclustY;
% corresponding initial mutual information terms
I_clustXclusty_initial = mutual_information(P_clustXclustY);
I_XclustY_initial = mutual_information(P_XclustY);
I_clustXY_initial = mutual_information(P_clustXY);

% initializations and for the sequential loops
k = 1; % iteration
diff = 1; % difference in cost between two subsequent iterations
Cost_k = zeros(numITER,1); % cost at each iteration
Cost_k(1) = -(factor_1minus2beta*(I_XclustY_initial + I_clustXY_initial) - factor_2minus2beta*I_clustXclusty_initial);
%%%%%%%%%%%%%%%%%%%%%%%% MAIN ITERATION %%%%%%%%%%%%%%%%%%%%%
while diff > tol && k < numITER
    % sequentially optimize clustering of Y (same function as for clustering of X can be used due to symmetry reasons)
    [ V_Y, I_XclustY, P_XclustY ] = sequential_opt_clustX( factor_1minus2beta, factor_2minus2beta, V_X, V_Y, P_XY' );
    % seuqentially optimize clustering of X
    [ V_X, I_clustXY, ~] = sequential_opt_clustX( factor_1minus2beta, factor_2minus2beta, V_Y, V_X, P_XY);
    % update necessary terms to evaluate the cost function 
    P_clustXclustY = V_X'*P_XclustY';
    I_clustXclustY = mutual_information(P_clustXclustY);
    % update cost function and calculate difference towards previous
    % iteration
    Cost_k(k+1) = -(factor_1minus2beta*(I_XclustY + I_clustXY) - factor_2minus2beta*I_clustXclustY);
    diff = abs(Cost_k(k+1) - Cost_k(k));
    k = k+1;
end

end